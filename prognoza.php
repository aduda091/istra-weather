<?php 

	$grad = $_GET['odabir'];
	if (!$grad) {
		die("Odaberite grad!");
	}

?>
<!DOCTYPE html>
<html lang="hr">
<head>
	<meta charset="UTF-8">
	<title>Istramet <?php echo ucwords($grad); ?></title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
<h1>Prognoza za <?php echo ucwords($grad); ?>:</h1>
	<table>
		<?php
		include('simple_html_dom.php');
		$link = "http://www.istramet.com/postaja/" . $grad . "/";
		//echo $link;
		$html = file_get_html($link);
		foreach($html->find('tr.red') as $e)
		    echo $e->outertext ;

		?>
	</table>
</body>
</html>