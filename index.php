<!DOCTYPE html>
<html lang="hr">
<head>
	<meta charset="UTF-8">
	<title>Istramet</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
<h1>Odaberite grad kojem želite vidjeti vremensku prognozu:</h1>
<form name="grad" action="prognoza.php" method="GET">
	<select name="odabir" id="odabir" autofocus>
		<option value="">Odaberite...</option>
	
		<?php
		include('simple_html_dom.php');

		$html = file_get_html('http://www.istramet.com/tablicni-prikaz-podataka/');

		foreach($html->find('tr.red td a') as $e) {
			$link = $e->href;
			$val = substr($link,strrpos($link,"/")+1);
		    echo "<option value=\"$val\">". $e->innertext . "</option>\n";
		}

		?>
	</select>
<input type="submit" value="Zasebna stranica" title="Prognoza u zasebnom prozoru, pogodno za bookmark">
</form>


<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
<script type="text/javascript">
$( document ).ready(function() {
    $("#odabir").change(function(){
    	var grad = "";
    	$( "select option:selected" ).each(function() {
      		grad += $( this ).val();
      		$("#preview").attr('src', "prognoza.php?odabir=" + grad);
    	});
    });
});
</script>

<iframe src="prognoza.php" id="preview" name="preview" frameborder="0"></iframe>


</body>
</html>